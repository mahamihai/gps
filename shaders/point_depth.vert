#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform float angle;
uniform float dance;
void main()
{
	vec4 worldPosition=model*vec4(aPos,1.0f);


    gl_Position =(dance>0) ? vec4(2*abs(sin(angle))*(vec3(1,1,1))+worldPosition.xyz,1.0f) : worldPosition;
}  