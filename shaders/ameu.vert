#version 330 core

layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
layout(location=2) in vec2 vTexCoords;
out vec3 normal1;

out vec2 fragTexCoords1;



uniform float dispersie;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceTrMatrix;
uniform float dance;
uniform vec3 cauldronCenter;
uniform float angle;
void main()
{
	

	normal1=vNormal;
	fragTexCoords1=vTexCoords;
	
	
	

	vec4 worldPosition=model*vec4(vPosition,1.0f);
	
	gl_Position=(dance>0) ? vec4(2*abs(sin(angle))*(vec3(1,1,1))+worldPosition.xyz,1.0f) : worldPosition;
}