#version 410 core

layout(location=0) in vec3 vPosition;

uniform mat4 lightSpaceTrMatrix;
uniform mat4 model;
uniform float angle;
uniform float dance;
void main()
{
vec4 worldPosition=model*vec4(vPosition,1.0f);
	vec4 temp=(dance>0) ? vec4(2*abs(sin(angle))*(vec3(1,1,1))+worldPosition.xyz,1.0f) : worldPosition;
    gl_Position = lightSpaceTrMatrix*temp;
}
