//
//  main.cpp
//  OpenGL Shadows
//
//  Created by CGIS on 05/12/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC
#include <vector>
#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include <stdio.h>
#include "stdafx.h"
#include <math.h>
#include "glm/ext.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#include "irrKlang.h"
//#include "particle_generator.hpp"
//#include "particle_generator.hpp"
#define TINYOBJLOADER_IMPLEMENTATION
//#include "irrklang\include\irrKlang.h"
//#pragma comment(lib, "irrKlang.lib")
//using namespace irrklang;
#include "Model3D.hpp"
#include "Mesh.hpp"
#include "stdafx.h"
int ok=2;
bool dir = true;

int glWindowWidth = 1280;
int glWindowHeight = 960;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

const GLuint SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;
float fov = 45;
glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::mat3 lightDirMatrix;
GLuint lightDirMatrixLoc;
glm::mat4 rot;
glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
GLuint rotatiaLoc;
gps::Camera myCamera(glm::vec3(-50.0f, 40.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f));
GLfloat cameraSpeed = 1.0f;
float dispersie = 30.0;
bool pressedKeys[1024];
GLfloat angle;
GLfloat directionSageata=0.0;
GLfloat directionNori=0.0;
GLfloat aux = 0.0f;
GLfloat lightAngle;
gps::Model3D wc;
bool done_dispersion = false;
float time1 = 0;
gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader depthMapShader;
gps::Shader slimShader;
gps::SkyBox mySkyBox;
gps::Shader skyboxShader;
gps::Shader pointDepth;
float last_time1 = 0;
glm::vec3 lightPos=glm::vec3(2,1,0);
gps::Model3D cazan;
gps::Model3D arm1;
gps::Model3D arm2;
gps::Model3D cap;
gps::Model3D temp;
gps::Model3D wolfenstein;
//gps::ParticleGenerator *generator;
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

irrklang::ISoundEngine *SoundEngine = irrklang::createIrrKlangDevice();
GLuint depthMapFBO;
GLuint depthMapTexture;
GLuint textura;
std::vector<const GLchar*> faces;
GLuint shadowMapFBO;
GLfloat yaw = 50.0f;
GLfloat pitch = 0.0f;
GLfloat lastX = glWindowWidth / 2.0f;
GLfloat lastY = glWindowHeight / 2.0f;
bool firstMouse;
bool start_walk = false;


// sound

//ISoundEngine *SoundEngine = createIrrKlangDevice();
std::vector <gps::Model3D> models;
std::map <std::string,gps::Model3D> specialModels;


GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	myCustomShader.useShaderProgram();

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
	
	lightShader.useShaderProgram();
	
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = (GLfloat)xpos;
		lastY = (GLfloat)ypos;
		firstMouse = false;
	}

	GLfloat xoffset = (GLfloat)xpos - lastX;
	GLfloat yoffset = lastY - (GLfloat)ypos;
	lastX = (GLfloat)xpos;
	lastY = (GLfloat)ypos;

	GLfloat sensitivity = 0.5f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;


	myCamera.rotate(pitch, yaw);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	
	if (fov >= 1.0f && fov <= 45.0f)
		fov -= yoffset;
	if (fov <= 1.0f)
		fov = 1.0f;
	if (fov >= 45.0f)
		fov = 45.0f;
	printf("%f AM ajuns aici\n",fov);

	
	

}
float rotation = 0;
float last_time=0;

void processMovement()
{
	float current_time = glfwGetTime();
	float deltat = 5*(current_time - last_time);
	last_time = current_time;


	
	if (pressedKeys[GLFW_KEY_R]) {
		
		
		rotation += 5;
		
		
	}


	if (pressedKeys[GLFW_KEY_Q]) {
		angle += deltat;
		if (angle > 360.0f)
			angle -= 360.0f;
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angle -= deltat;
		if (angle < 0.0f)
			angle += 360.0f;
	}

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
		//cameraPos += cameraSpeed * cameraFront;
	}
	if (pressedKeys[GLFW_KEY_J]) {
		start_walk = true;
	}
	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_Z])
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (pressedKeys[GLFW_KEY_O])
	{
		dispersie =dispersie+ 0.1;
		
		std::cout << dispersie << " \n";
		
	}
	if (pressedKeys[GLFW_KEY_P])
	{
		
			dispersie = dispersie - 0.1;
			if (dispersie<0)
				dispersie=0;
		std::cout << dispersie << " \n";

	}
	if (pressedKeys[GLFW_KEY_X])
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (pressedKeys[GLFW_KEY_J]) {
		ok = 1;
		
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle -= 0.3f; 
		if (lightAngle < 0.0f)
			lightAngle += 360.0f;
		//glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		lightDir= glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));
	}	
}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
    //glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetScrollCallback(glWindow, scroll_callback);
	return true;
}

void initOpenGLState()
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	//glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
	
}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);


	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void draw_model(gps::Model3D object, gps::Shader shader)
{
	
	object.Draw(shader);
}
void render_models(gps::Shader shader)
{
	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniform3fv(glGetUniformLocation(shader.shaderProgram, "cauldronCenter"), 1, glm::value_ptr(cazan.get_center()));

	glUniform1f(glGetUniformLocation(shader.shaderProgram, "angle"),angle);
	glUniform1f(glGetUniformLocation(shader.shaderProgram, "dance"), 1);

	for (size_t i = 0; i < models.size(); i++)
	{
		gps::Model3D currentModel = models.at(i);
		
		currentModel.Draw(shader);


	}
	glUniform1f(glGetUniformLocation(shader.shaderProgram, "dance"), 0);
	gps::Model3D temp;
	for (std::map<std::string, gps::Model3D>::iterator it = specialModels.begin(); it != specialModels.end(); it++)
	{
		if (it->first.compare("wolfenstein")==0)
		{
			it->second.setModel(glm::rotate(it->second.getModel(), glm::radians(-angle), glm::vec3(0, 1, 0)));
		}
		temp = it->second;
		glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(temp.getModel()));
		temp.Draw(shader);
	}


}


void initModels()
{
	
	
	//teemo = gps::Model3D("objects/Teemo/Teemo.obj", "objects/Teemo/");

	//models.push_back(teemo);
	models.push_back(gps::Model3D("objects/clown/clovn.obj", "objects/clown/"));
	
		wolfenstein=gps::Model3D("objects/wolf/WOlfenstein.obj", "objects/Wolf/");
		specialModels.insert(std::pair<std::string, gps::Model3D>("wolfenstein", wolfenstein));
			models.push_back(gps::Model3D("objects/warrior/ZombieWarrior.obj", "objects/warrior/"));

			arm1=gps::Model3D("objects/arm/arm.obj", "objects/arm/");
			arm2=gps::Model3D("objects/arm/arm1.obj", "objects/arm/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("arm1", arm1));
			specialModels.insert(std::pair<std::string, gps::Model3D>("arm2", arm2));

			cap=gps::Model3D("objects/head/TheRock2.obj", "objects/head/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cap", cap));
			temp = gps::Model3D("objects/sarcofag/acroportrait.obj", "objects/sarcofag/");
			
			specialModels.insert(std::pair<std::string, gps::Model3D>("sarcofag", temp));
			
			temp = gps::Model3D("objects/alien/alien.obj", "objects/alien/");

			specialModels.insert(std::pair<std::string, gps::Model3D>("alien", temp));
			temp = gps::Model3D("objects/cross/cross.obj", "objects/cross/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cross", temp));

			temp = gps::Model3D("objects/cross/cross1.obj", "objects/cross/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cross1", temp));

			temp = gps::Model3D("objects/cross/cross2.obj", "objects/cross/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cross2", temp));

			temp = gps::Model3D("objects/cross/cross3.obj", "objects/cross/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cross3", temp));

			temp = gps::Model3D("objects/cross/cross4.obj", "objects/cross/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cross4", temp));








			models.push_back(gps::Model3D("objects/ogre/ogre.obj", "objects/ogre/"));
			cazan=gps::Model3D("objects/cauldron/cauldron.obj", "objects/cauldron/");
			specialModels.insert(std::pair<std::string, gps::Model3D>("cazan", cazan));

	models.push_back(gps::Model3D("objects/SLENDER/Slenderman.obj", "objects/SLENDER/"));
	//lightPos = pumpkin.get_center();
	models.push_back(gps::Model3D("objects/wendigo/Wendigo.obj", "objects/wendigo/"));

	gps::Model3D ground=gps::Model3D("objects/ground/pamantu.obj", "objects/ground/");
	specialModels.insert(std::pair<std::string, gps::Model3D>("ground", ground));
	//models.push_back(gps::Model3D("objects/sfera/sfera.obj", "objects/sfera/"));


}

void initShaders()
{
	//pointDepth.loadShader1("shaders/point_depth/vert", "shaders/point_depth/frag", "shaders/point_depth/gdp");
	myCustomShader.loadShader1("shaders/ameu.vert", "shaders/ameu.frag", "shaders/spargere.gdp");
	//lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	depthMapShader.loadShader1("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag", "shaders/simpleDepthMap.gdp");
	skyboxShader.loadShader("shaders/cer.vert", "shaders/cer.frag");
	slimShader.loadShader("shaders/particle.vert", "shaders/particle.frag");
}

void initUniforms()
{
	myCustomShader.useShaderProgram();

	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");

	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	
	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");
	glm::mat4 view;

	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	lightDirMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDirMatrix");

	projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 10000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));	
	glUniformMatrix4fv(glGetUniformLocation(slimShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir =  lightPos;
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");

	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 0.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	
}
glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = 1.0f, far_plane = 10.0f;
	glm::mat4 lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);

		//glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);
		//glm::perspective(glm::radians(30.0f), (float)retina_width / (float)retina_height, 0.1f, 10000.0f);

	glm::mat4 lightView = glm::lookAt(lightDir, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}
void set_sky()
{
	std::vector<const GLchar*> faces;
	faces.push_back("textures/skybox/darkskies_rt.tga");
	faces.push_back("textures/skybox/darkskies_lf.tga");
	faces.push_back("textures/skybox/darkskies_up.tga");
	faces.push_back("textures/skybox/darkskies_dn.tga");
	faces.push_back("textures/skybox/darkskies_bk.tga");
	faces.push_back("textures/skybox/darkskies_ft.tga");
	mySkyBox.Load(faces);

}
void draw_dept_shadow()
{
	depthMapShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);

	glClear(GL_DEPTH_BUFFER_BIT);

	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	

	
	render_models(depthMapShader);


}
GLuint createShader(GLenum type, const GLchar* src) {
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);
	return shader;
}
float timp_particula=0;
float timp_particula1=0;
void generate_cube_map()
{
	unsigned int depthCubemap;
	glGenTextures(1, &depthCubemap);
	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);
	for (unsigned int i = 0; i < 6; ++i)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT,
			SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubemap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	//setup point depth shader
	
}
void render_light()
{
	/*
	float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
	float near_plane = 1.0f;
	float far_plane = 25.0f;
	glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT, near_plane, far_plane);
	glUniformMatrix4fv(glGetUniformLocation(pointDepth.shaderProgram,
		"shadowProjection"),
		1,
		GL_FALSE,
		glm::value_ptr(shadowProj));
	glm::vec3 lightPos = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));

	std::vector<glm::mat4> shadowTransforms;
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	pointDepth.useShaderProgram();
	for (unsigned int i = 0; i < 6; ++i)
		//seteaza matricele <<<<<<
		pointDepth.setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
	pointDepth.setFloat("far_plane", far_plane);
	pointDepth.setVec3("lightPos", lightPos);
	renderScene(pointDepth);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	*/

}

void renderScene()
{


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	processMovement();

	model = glm::mat4(1);
	for (std::map<std::string, gps::Model3D>::iterator it = specialModels.begin(); it != specialModels.end(); it++)
	{
		it->second.setModel(model);
	}

	model = glm::translate(model, -cazan.get_center());
	model = glm::rotate(model, glm::radians(10*angle), glm::vec3(0, 1, 0));
	
	//model = glm::rotate(model, glm::radians(rotation), glm::vec3(0, 1, 0));
	model = glm::translate(model, cazan.get_center());
	time1 = glfwGetTime();
	std::cout << "dispersia este " << dispersie << "\n";
	if (!done_dispersion&&start_walk)
	{
		if (dispersie > 0)
		{
			
			dispersie-= 0.3;
		
			if (time1 - last_time1 > 0.05)
			{
				myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
			}
		}
		else
		{
			dispersie = 0;
			done_dispersion = true;
		}
	}
	
	
	
	GLint timeLoc = glGetUniformLocation(depthMapShader.shaderProgram, "time");
	glUniform1f(timeLoc, dispersie);
	//model = ret*model;
	//model = glm::scale(model, glm::vec3(60));
	angle += 5*(time1 - last_time1);
	/////////////
	draw_dept_shadow();
	/////////////
	

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	myCustomShader.useShaderProgram();
	projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 10000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	 timeLoc = glGetUniformLocation(myCustomShader.shaderProgram, "time");
	glUniform1f(timeLoc, dispersie);

	

	//send view matrix to shader
	
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
	//compute light direction transformation matrix
	lightDirMatrix = glm::mat3(glm::inverseTranspose(view));
	//send lightDir matrix data to shader
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));
	glViewport(0, 0, retina_width, retina_height);


	myCustomShader.useShaderProgram();

	//bind the depth map
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);
	
	//model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));

	// model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1, 0, 0));
	//send model matrix data to shader	
	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	
	render_models(myCustomShader);
	//pumpkin.Draw(myCustomShader);
	

	//create normal matrix
	//send normal matrix data to shader
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));

	mySkyBox.Draw(skyboxShader, view, projection);
	

	glUniformMatrix4fv(glGetUniformLocation(slimShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(slimShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	timp_particula1 = glfwGetTime();
	//glm::vec4 center= view * model* glm::vec4(teemo.get_center(), 1.0f);

	
	last_time1 = time1;
}

int main(int argc, const char * argv[]) {

	

	//myCustomShader.loadShader("shaders/ameu.vert", "shaders/ameu.frag");

	initOpenGLWindow();
	initOpenGLState();
	initFBOs();
	initModels();
	//glEnable(GL_FRAMEBUFFER_SRGB);
	set_sky();
	initShaders();
	
	//generate_cube_map();
	initUniforms();	
	glCheckError();
	glGenTextures(1, &textura);
	//generator = new gps::ParticleGenerator(slimShader, 400);
	glBindTexture(GL_TEXTURE_2D, textura);
	//generator =new gps::ParticleGenerator(slimShader, 50);
	last_time1 = glfwGetTime();
	time1 = glfwGetTime();
	SoundEngine->play2D("music/demon.mp3", GL_TRUE);
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}
