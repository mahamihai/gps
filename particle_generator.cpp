/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include "particle_generator.hpp"
#include "stdafx.h"
namespace gps {

	ParticleGenerator::ParticleGenerator(Shader shader, GLuint amount)
		: shader(shader), amount(amount)
	{
		this->init();
	}

	void ParticleGenerator::Update(GLfloat dt, glm::vec3 object, GLuint newParticles, glm::vec2 offset)
	{
		// Add new particles 
		for (GLuint i = 0; i < newParticles; ++i)
		{
			int unusedParticle = this->firstUnusedParticle();
			this->respawnParticle(this->particles[unusedParticle], object, offset);
			std::cout << "Found particle " << unusedParticle << "\n";

		}
		// Update all particles
		for (GLuint i = 0; i < this->amount; ++i)
		{
			Particle &p = this->particles[i];
			p.Life -= dt; // reduce life
			if (p.Life > 0.0f)
			{	// particle is alive, thus update
				p.Position = p.Velocity * dt;
				p.Color.a -= dt * 2.5;
			}
		}
	}

	// Render all particles
	void ParticleGenerator::Draw()
	{
		// Use additive blending to give it a 'glow' effect
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		this->shader.useShaderProgram();
		for (Particle particle : this->particles)
		{
			if (particle.Life > 0.0f)
			{
				glUniform2fv( glGetUniformLocation(this->shader.shaderProgram, "offset"),1,glm::value_ptr(particle.Position));
				glUniform4fv(glGetUniformLocation(this->shader.shaderProgram, "color"),1, glm::value_ptr(particle.Color));

				//this->shader.SetVector2f("offset", particle.Position);
				//this->shader.SetVector4f("color", particle.Color);
				//this->texture.Bind();
				glBindVertexArray(this->VAO);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glBindVertexArray(0);
			}
		}
		// Don't forget to reset to default blending mode
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void ParticleGenerator::init()
	{
		// Set up mesh and attribute properties
		GLuint VBO;
		GLfloat particle_quad[] = {
			0.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,

			0.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f
		};
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(this->VAO);
		// Fill mesh buffer
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);
		// Set mesh attributes
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
		glBindVertexArray(0);

		// Create this->amount default particle instances
		for (GLuint i = 0; i < this->amount; ++i)
			this->particles.push_back(Particle());
	}

	// Stores the index of the last particle used (for quick access to next dead particle)
	GLuint lastUsedParticle = 0;
	GLuint ParticleGenerator::firstUnusedParticle()
	{
		// First search from last used particle, this will usually return almost instantly
		for (GLuint i = lastUsedParticle; i < this->amount; ++i) {
			if (this->particles[i].Life <= 0.0f) {
				lastUsedParticle = i;
				return i;
			}
		}
		// Otherwise, do a linear search
		for (GLuint i = 0; i < lastUsedParticle; ++i) {
			if (this->particles[i].Life <= 0.0f) {
				lastUsedParticle = i;
				return i;
			}
		}
		// All particles are taken, override the first one (note that if it repeatedly hits this case, more particles should be reserved)
		lastUsedParticle = 0;
		return 0;
	}

	void ParticleGenerator::respawnParticle(Particle &particle, glm::vec3 object, glm::vec2 offset)
	{
		GLfloat random = ((rand() % 100) - 50) / 10.0f;
		GLfloat rColor = 0.5 + ((rand() % 100) / 100.0f);
		particle.Position = glm::vec2(object.x, object.y) + random + offset;
		particle.Color = glm::vec4(rColor, 0, 0, 1.0f);
		particle.Life = 4.0f;
		particle.Velocity =glm::vec2((rand()%10-5)/10,(rand()%4 -1.5)/10);
	}


}