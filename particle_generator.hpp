/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#ifndef PARTICLE_GENERATOR_H
#define PARTICLE_GENERATOR_H
#include <vector>
#include "GLEW/glew.h"
#include "glm/glm.hpp"

#include "Shader.hpp"
#include "GLFW/glfw3.h"

#include <GL/gl.h>
#include <GL/glu.h>


#include "stdafx.h"
#include "glm/ext.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "Mesh.hpp"

namespace gps {
	// Represents a single particle and its state
	struct Particle {
		glm::vec2 Position, Velocity;
		glm::vec4 Color;
		GLfloat Life;

		Particle() : Position(0.0f), Velocity(0.0f), Color(1.0f), Life(0.0f) { }
	};


	// ParticleGenerator acts as a container for rendering a large number of 
	// particles by repeatedly spawning and updating particles and killing 
	// them after a given amount of time.
	class ParticleGenerator
	{
	public:
		// Constructor
		ParticleGenerator(Shader shader, GLuint amount);
		// Update all particles
		void Update(GLfloat dt, glm::vec3 object, GLuint newParticles, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
		// Render all particles
		void Draw();
	private:
		// State
		std::vector<Particle> particles;
		GLuint amount;
		// Render state
		Shader shader;
		Texture texture;
		GLuint VAO;
		// Initializes buffer and vertex attributes
		void init();
		// Returns the first Particle index that's currently unused e.g. Life <= 0.0f or 0 if no particle is currently inactive
		GLuint firstUnusedParticle();
		// Respawns particle
		void respawnParticle(Particle &particle, glm::vec3 object, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
	};
}
#endif